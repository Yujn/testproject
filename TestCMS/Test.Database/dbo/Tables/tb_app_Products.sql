﻿CREATE TABLE [dbo].[tb_app_Products] (
    [Id]            UNIQUEIDENTIFIER NOT NULL default(newId()),
	[CategoryId]	UNIQUEIDENTIFIER constraint fk_Product_CategoryId references tb_app_Categories(Id) NOT NULL,
    [Name]          VARCHAR (512)    NOT NULL,
    [Description]   VARCHAR (MAX)    NULL,
    [Price]         DECIMAL (18,2)     NOT NULL,
	[Slug]			varchar(512)	Not null,
    [DateTimeStamp] DATETIME         DEFAULT (getdate()) NOT NULL,
    [Cancelled]     BIT              DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

