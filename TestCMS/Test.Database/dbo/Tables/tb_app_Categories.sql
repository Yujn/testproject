﻿CREATE TABLE [dbo].[tb_app_Categories]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY default(newId()),
	[Name] varchar(512) not null,
	DateTimeStamp DateTime not null default(getDate()),
	[Cancelled] bit not null default(0)
)
