﻿CREATE TABLE [dbo].[tb_core_Errors]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY default(newId()),
	[Message] varchar(MAX),
	[InnerException] varchar(MAX),
	[StackTrace] varchar(MAX),
	[DateTimeStamp] DateTime not null default(0)
)
