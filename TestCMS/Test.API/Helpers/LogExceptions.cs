﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.API.Helpers
{
    public static class LogExceptions
    {
        public static void LogException(Exception e)
        {
            using (Data.TestContext context = new Data.TestContext())
            {
                Data.TbCoreError error = new Data.TbCoreError()
                {
                    InnerException = e.InnerException == null ? "" : e.InnerException.Message,
                    Message = e.Message,
                    StackTrace = e.StackTrace
                };

                context.TbCoreErrors.Add(error);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //serious problems if this is run hit. Just here for safty
                }
            }
        }
    }
}