﻿using System;
using System.Web;
using System.Web.Http;
using System.Linq;
using System.Collections.Generic;
using Common;
using System.Web.Http.Description;
using Common.Models.Common;
using Common.Models.Category;
using System.Net;

namespace Test.API.Areas.Secure
{
    public class CategoriesController : ApiController
    {
        private const string _ = Defaults.AppId + "-api/" + Defaults.SecureAreaId + "/category/";

        [HttpGet]
        [Route(_)]
        [ResponseType(typeof(List<KeyValuePair>))]
        public List<KeyValuePair> GetBasicCategories()
        {                        
            using (Data.TestContext context = new Data.TestContext())
            {
                return context.TbAppCategories.Where(x => !x.Cancelled)
                                              .Select(x => new KeyValuePair
                                              {
                                                  Id = x.Id,
                                                  Name = x.Name
                                              })
                                              .OrderByDescending(x=>x.Name).ToList();
            }            
        }

        //This is just for adding meta data for the purpose of this project
        [HttpPost]
        [Route(_)]
        [ResponseType(typeof(HttpStatusCode))]
        public HttpStatusCode CreateCategory(RequestCategory request)
        {
            using (Data.TestContext context = new Data.TestContext())
            {                
                Data.TbAppCategory category = new Data.TbAppCategory()
                {
                    Name = request.Name
                };

                context.TbAppCategories.Add(category);

                try
                {
                    context.SaveChanges();
                    return HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    Helpers.LogExceptions.LogException(ex);
                    return HttpStatusCode.BadRequest;
                }
            }
        }
    }
}
