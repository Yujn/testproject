﻿using Common.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.API.Areas.Secure.Classes
{
    public static class Product
    {
        public static ResponseProduct GetProduct(string slug)
        {
            using (Data.TestContext context = new Data.TestContext())
            {
                return context.TbAppProducts.Where(x => !x.Cancelled &&
                                                        x.Slug == slug)
                                            .Select(x => new ResponseProduct
                                            {
                                                Id = x.Id,
                                                Name = x.Name,
                                                Description = x.Description,
                                                Price = x.Price,
                                                Slug = x.Slug,
                                                CategoryId = x.CategoryId,
                                                CategoryName = x.Name
                                            }).FirstOrDefault();
            }
        }
    }
}