﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Common.Models.Product;

namespace Test.API.Areas.Secure
{
    public class ProductsController : ApiController
    {
        private const string _ = Defaults.AppId + "-api/" + Defaults.SecureAreaId + "/product/";

        [HttpGet]
        [Route(_)]
        [ResponseType(typeof(List<ResponseProduct>))]
        public List<ResponseProduct> GetProducts()
        {
            using (Data.TestContext context = new Data.TestContext())
            {
                return context.TbAppProducts.Where(x => !x.Cancelled)
                                            .Select(x => new ResponseProduct
                                            {
                                                Id = x.Id,
                                                Name = x.Name,
                                                Description = x.Description,
                                                Price = x.Price,
                                                CategoryName = x.TbAppCategory.Name
                                            }).ToList();
            }
        }

        [HttpGet]
        [Route(_ + "{slug}")]
        [ResponseType(typeof(ResponseProduct))]
        public ResponseProduct GetProduct(string slug)
        {
            return Classes.Product.GetProduct(slug);
        }

        [HttpPost]
        [Route(_)]
        [ResponseType(typeof(ResponseProduct))]
        public ResponseProduct CreateProduct(RequestProduct request)
        {
            using (Data.TestContext context = new Data.TestContext())
            {
                string slug = Helpers.SlugHelper.GenerateSlug(request.Name);

                Data.TbAppProduct product = new Data.TbAppProduct()
                {
                    Name = request.Name,
                    Description = request.Description,
                    Price = request.Price,
                    CategoryId = request.CategoryId,
                    Slug = slug
                };

                context.TbAppProducts.Add(product);

                try
                {
                    context.SaveChanges();
                    return Classes.Product.GetProduct(slug);                    
                }
                catch (Exception ex)
                {
                    Helpers.LogExceptions.LogException(ex);
                    return new ResponseProduct()
                    {
                        Name = request.Name,
                        Description = request.Description,
                        Price = request.Price,                        
                        CategoryId = request.CategoryId                        
                    };
                }
            }
        }

        //HttpPut if using restful api
        //Would also change this to use an ID just to be more specific on what is been removed.
        [HttpPost]
        [Route(_ + "{slug}")]
        [ResponseType(typeof(HttpStatusCode))]
        public HttpStatusCode UpdateProduct(string slug, RequestProduct request)
        {
            using (Data.TestContext context = new Data.TestContext())
            {
                try
                {
                    var product = context.TbAppProducts.Where(x => !x.Cancelled &&
                                                                    x.Slug == slug)
                                                       .FirstOrDefault();

                    if (product == null)
                    {
                        return HttpStatusCode.NotFound;
                    }

                    product.Name = request.Name;
                    product.Price = request.Price;
                    product.Description = request.Description;
                    product.CategoryId = request.CategoryId;

                    context.SaveChanges();

                    return HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    Helpers.LogExceptions.LogException(ex);
                    return HttpStatusCode.BadRequest;
                }
            }
        }

        //httpDelete if using restful api
        //Would also change this to use an ID just to be more specific on what is been removed.
        [HttpPost]
        [Route(_ + "delete/{slug}")]
        [ResponseType(typeof(HttpStatusCode))]
        public HttpStatusCode DeleteProduct(string slug)
        {
            using (Data.TestContext context = new Data.TestContext())
            {
                if (string.IsNullOrEmpty(slug))
                {
                    return HttpStatusCode.BadRequest;                    
                }

                var product = context.TbAppProducts.Where(x => !x.Cancelled && 
                                                                  x.Slug == slug)
                                                   .FirstOrDefault();

                if (product == null)
                {
                    return HttpStatusCode.NotFound;                    
                }

                bool isCancelled = false;

                try
                {
                    product.Cancelled = true;
                    context.SaveChanges();
                    isCancelled = true;

                    try
                    {
                        context.TbAppProducts.Remove(product);
                        context.SaveChanges();
                        return HttpStatusCode.OK;
                    }
                    catch (Exception ex)
                    {
                        if (isCancelled)
                        {
                            return HttpStatusCode.OK;
                        }
                        else
                        {
                            Helpers.LogExceptions.LogException(ex);
                            return HttpStatusCode.BadRequest;                            
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    Helpers.LogExceptions.LogException(ex);
                    return HttpStatusCode.BadRequest;
                }
            }
        }
    }
}
