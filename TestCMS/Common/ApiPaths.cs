﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ApiPaths
    {
        public static class Products
        {
            private const string _ = Defaults.AppId + "-api/" + nameof(Products) + "/";

            public const string GetProducts = _ + nameof(GetProducts);
            public const string GetProductBySlug = _ + nameof(GetProductBySlug);
            //public const string  = _ + nameof(GetProducts);
        }

        public static class Categories
        {
            private const string _ = Defaults.AppId + "-api/" + nameof(Categories) + "/";

            public const string GetCategories = _ + nameof(GetCategories);
        }
    }
}
