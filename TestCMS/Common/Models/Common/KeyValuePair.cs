﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Common
{
    public class KeyValuePair
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
