﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Defaults
    {
        public const string AppId = "app";

        public const string SecureAreaId = "Secure";

        public const string PublicAreaId = "public";
    }
}
